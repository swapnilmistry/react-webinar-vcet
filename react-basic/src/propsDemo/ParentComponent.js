import ChildComponent from "./ChildComponent";

function ParentComponent() {
  let a = "Prop from parent component";
  return (
    <div>
      <ChildComponent a={a} />
      <p>Hello for parent component</p>
    </div>
  );
}

export default ParentComponent;

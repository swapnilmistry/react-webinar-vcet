import React, { useState, useRef } from "react";
import { Form, FormGroup, Label, Input, Button, Alert } from "reactstrap";
import "./FormDemo.css";

function FormDemo() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [visible, setVisible] = useState(false);
  const [alertColor, setAlertColor] = useState("info");
  const alertMessage = useRef("");

  let onEmailChange = (event) => {
    setEmail(event.target.value);
  };

  let onPasswordChange = (event) => {
    setPassword(event.target.value);
  };

  let onSubmit = (event) => {
    event.preventDefault();
    if (!email || !password) {
      setVisible(true);
      setAlertColor("danger");
      alertMessage.current =
        "It seems password or email is empty. Please enter them before submitting the form.";
    } else {
      setVisible(true);
      setAlertColor("success");
      alertMessage.current = "Form submitted successfully.";
    }
  };

  let onDismiss = () => {
    setVisible((x) => !x);
  };

  let resetForm = () => {
    setEmail("");
    setPassword("");
  };

  return (
    <div className={"form-container"}>
      <Form>
        <FormGroup className={"form-group"}>
          <Label className={"form-label"}>Email</Label>
          <Input
            type="email"
            name="email"
            placeholder="Enter Email"
            value={email}
            onChange={onEmailChange}
          />
        </FormGroup>
        <FormGroup className={"form-group"}>
          <Label className={"form-label"}>Password</Label>
          <Input
            type="password"
            name="password"
            placeholder="Enter Password"
            value={password}
            onChange={onPasswordChange}
          />
        </FormGroup>
        <div className={"action-buttons"}>
          <Button color="primary" size="sm" onClick={onSubmit}>
            Submit
          </Button>
          <Button color="danger" size="sm" onClick={resetForm}>
            Reset
          </Button>
        </div>
      </Form>
      <Alert
        className={"alert-action"}
        color={alertColor}
        isOpen={visible}
        toggle={onDismiss}
        fade={true}
      >
        {alertMessage.current}
      </Alert>
    </div>
  );
}

export default FormDemo;

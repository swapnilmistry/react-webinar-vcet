const todoInputPlaceHolderMessage = "Please enter todo";
const addTodoButtonText = "Add Todo";
const removeTodoButtonText = "Remove Todo";
const noTodosFound = "Tada! Not todos for the day";

export {
  todoInputPlaceHolderMessage,
  addTodoButtonText,
  removeTodoButtonText,
  noTodosFound,
};
